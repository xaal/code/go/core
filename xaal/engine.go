package xaal

import (
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"reflect"
	"slices"
	"time"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
)

const (
	EngineStopped EngineState = 0
	EngineStarted EngineState = 1
	EngineRunning EngineState = 2
)

type EngineState int

type Engine struct {
	NetworkConnector *NetworkConnector
	MessagesFactory  *MessageFactory
	AttributesChange *Queue
	Subscribers      []func(msg *Message)
	Devices          []*Device
	State            EngineState
}

func NewEngine() *Engine {
	// read the config file
	config, err := GetConfig()
	if err != nil {
		panic(err)
	}
	// create the message factory
	mf, err := NewMessageFactory(config.KeyString)
	if err != nil {
		panic(err)
	}
	// create the network connector
	nc, err := NewNetworkConnector(config.Address, config.Port, config.BindAddress, config.TTL)
	if err != nil {
		panic(err)
	}

	attrsChange := NewQueue()
	eng := &Engine{
		NetworkConnector: nc,
		MessagesFactory:  mf,
		AttributesChange: attrsChange,
		State:            EngineStopped,
	}
	// setup the default message filter
	eng.SetMessageFilter(eng.defaultFilter)
	// subscribe to receive messages
	eng.Subscribe(eng.handleReceiveMSG)
	return eng
}

//==============================================================================================
// Start / Stop / Miscs
//==============================================================================================

// start goroutines
func (eng *Engine) Start() {
	if eng.State != EngineStopped {
		slog.Error("Engine already started")
		return
	}
	go eng.NetworkConnector.Receive()
	go eng.processReceiveQueue()
	go eng.processAttributesChange()
	go eng.processSendAlives()
	eng.State = EngineStarted
	slog.Info("Engine started")
}

// just disconnect
func (eng *Engine) Stop() {
	eng.NetworkConnector.Stop()
	slog.Info("Engine stopped")
	eng.State = EngineStopped
}

// run forever, until we get a signal
func (eng *Engine) Run() {
	if eng.State != EngineStarted {
		eng.Start()
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	// stop
	fmt.Print("\r")
	slog.Info("Exiting")
	eng.Stop()
}

//==============================================================================================
// Goroutines
//==============================================================================================

// read de network queue
func (eng *Engine) processReceiveQueue() {
	for {
		pkt := eng.NetworkConnector.queue.Pop()
		if pkt != nil {
			// Decode the new pkt
			msg, err := eng.MessagesFactory.DecodeMSG(pkt.([]byte))
			// Check if the Message is valid
			if err != nil {
				slog.Error("DecodeMSG", "err", err)
			} else {
				// We received a valid (and filtered) message, let's
				// publish it to the Subscribers
				if msg != nil {
					eng.Publish(msg)
				}
			}
		} else {
			time.Sleep(50 * time.Millisecond)
		}
	}
}

// wait for AttributesChange and send them
func (eng *Engine) processAttributesChange() {
	for {
		if eng.AttributesChange.Len() == 0 {
			time.Sleep(100 * time.Millisecond)
		} else {
			attrs := eng.AttributesChange.Flush()
			target := make(map[*Device][]*Attribute)
			for _, attr := range attrs {
				dev := attr.(*Attribute).Device
				target[dev] = append(target[dev], attr.(*Attribute))
			}
			for dev, attrs := range target {
				eng.SendAttributesChange(dev, attrs)
			}
		}
	}
}

// Periodically send alive message for device that should
// As we send alive at timeout/2, we can spend some extra
// delay to limit the needed processing.
func (eng *Engine) processSendAlives() {
	for {
		time.Sleep(time.Second * 2)
		for _, dev := range eng.Devices {
			if dev.ShouldEmitAlive() {
				eng.SendAlive(dev)
			}
		}
	}
}

//==============================================================================================
// Process Incomming
//==============================================================================================

// search if incomming message is for us and call handleRequest if needed
// handleReceiveMSG works through the pub/sub system
func (eng *Engine) handleReceiveMSG(msg *Message) {
	for _, dev := range eng.Devices {
		if msg.IsIsAlive() {
			// send alive if we are the target or if it's a broadcast
			if slices.Contains(msg.Targets, dev.Address) || slices.Contains(msg.Targets, uuid.Nil) {
				if value, ok := msg.Body["dev_types"]; ok {
					if dev.MatchDevTypes(decodeSlice(value)) {
						eng.SendAlive(dev)
					}
				}
			}
			continue
		}
		// handle request if we are the target
		if msg.IsRequest() && slices.Contains(msg.Targets, dev.Address) {
			eng.handleRequest(dev, msg)
		}
	}
}

// call a Message.Action on a given Device
func (eng *Engine) handleRequest(dev *Device, msg *Message) {
	fn := dev.GetMethod(msg.Action)
	if fn != nil {
		result := fn.(func(MessageBody) *MessageBody)(msg.Body)
		if result != nil {
			targets := make([]uuid.UUID, 1)
			targets[0] = msg.Source
			eng.SendReply(dev, targets, msg.Action, *result)
		}
	}
}

//==============================================================================================
// Subscribers
//==============================================================================================

// subscribe to receive messages
func (eng *Engine) Subscribe(fn func(msg *Message)) {
	eng.Subscribers = append(eng.Subscribers, fn)
}

// unsubscribe
func (eng *Engine) UnSubscribe(fn func(msg *Message)) {
	for i, f := range eng.Subscribers {
		// This is a pointer comparison in Go
		if reflect.ValueOf(f).Pointer() == reflect.ValueOf(fn).Pointer() {
			eng.Subscribers = append(eng.Subscribers[:i], eng.Subscribers[i+1:]...)
		}
	}
}

// forward messages to all subscribers
func (eng *Engine) Publish(msg *Message) {
	for _, fn := range eng.Subscribers {
		fn(msg)
	}
}

// ==============================================================================================
// Message Filtering
// ==============================================================================================

// Set the message filter function.
func (eng *Engine) SetMessageFilter(fn func(*Message) bool) {
	eng.MessagesFactory.SetFilter(fn)
}

// Disable the message filter function.
func (eng *Engine) DisableMessageFilter() {
	// Set a filter that always return true.
	eng.MessagesFactory.SetFilter(eng.disabledFilter)
}

// The default filter function, check if the message is for know device
func (eng *Engine) defaultFilter(msg *Message) bool {
	// This is probably a isAlive request, we're not sure
	// at this early stage of decoding, so give it a try
	if slices.Contains(msg.Targets, uuid.Nil) {
		return true
	}
	// Is this a device we know ?
	for _, dev := range eng.Devices {
		if slices.Contains(msg.Targets, dev.Address) {
			return true
		}
	}
	return false
}

// The disabled filter function, always return true.
// This should be used if you want to receive all the messages.
func (eng *Engine) disabledFilter(msg *Message) bool {
	return true
}

//==============================================================================================
// Devices & Attributes
//==============================================================================================

// register a device
func (eng *Engine) AddDevice(dev *Device) {
	eng.Devices = append(eng.Devices, dev)
	dev.Engine = eng
	eng.SendAlive(dev)
}

// unregister a device
func (eng *Engine) RemoveDevice(dev *Device) {
	for i, d := range eng.Devices {
		if d == dev {
			dev.Engine = nil
			eng.Devices = append(eng.Devices[:i], eng.Devices[i+1:]...)
		}
	}
}

// queue an attribute change
func (eng *Engine) QueueAttributeChange(attr *Attribute) {
	slog.Debug("QueueAttributeChange", "attr", attr)
	eng.AttributesChange.Push(attr)
}

//==============================================================================================
// Sending
//==============================================================================================

// send a message directly
func (eng *Engine) Send(msg *Message) {
	now := time.Now() // current local time
	msg.Timestamp[0] = uint64(now.Unix())
	msg.Timestamp[1] = uint64(now.Nanosecond() / 1000 % 1000000)
	pkt, err := eng.MessagesFactory.EncodeMSG(msg)
	if err != nil {
		slog.Error("EncodeMSG:", "err", err)
	} else {
		eng.NetworkConnector.Send(pkt)
	}
}

// send a reply
func (eng *Engine) SendReply(dev *Device, targets []uuid.UUID, method string, body MessageBody) {
	msg := eng.MessagesFactory.BuildMessage(dev, targets, MGSTypeReply, method, body)
	eng.Send(msg)
}

// Send an alive message and update Device NextAlive
func (eng *Engine) SendAlive(dev *Device) {
	targets := make([]uuid.UUID, 0)
	body := make(MessageBody)
	timeout := MustGetConfig().AliveTimer
	body["timeout"] = timeout
	msg := eng.MessagesFactory.BuildMessage(dev, targets, MSGTypeNotify, string(MSGActionAlive), body)
	eng.Send(msg)
	dev.UpdateNextAlive(timeout / 2)
}

// send an attributes change
func (eng *Engine) SendAttributesChange(dev *Device, attrs []*Attribute) {
	body := make(MessageBody)
	for _, attr := range attrs {
		body[attr.Name] = attr.Value
	}
	msg := eng.MessagesFactory.BuildMessage(dev, []uuid.UUID{}, MSGTypeNotify, string(MSGActionAttributesChange), body)
	eng.Send(msg)
}

// send an notification
func (eng *Engine) SendNotification(dev *Device, notification string, body MessageBody) {
	msg := eng.MessagesFactory.BuildMessage(dev, []uuid.UUID{}, MSGTypeNotify, notification, body)
	eng.Send(msg)
}
