package xaal

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"sync"

	"gopkg.in/ini.v1"
)

const (
	defaultAddr         = "224.0.29.200"
	defaultPort         = 1236
	defaultBindAddr     = "0.0.0.0"
	defaultTTL          = 10
	defaultAliveTimer   = 500
	defaultLogLevel     = "INFO"
	defaultCipherWindow = 30
)

type Config struct {
	KeyString    string
	Address      string
	Port         int
	BindAddress  string
	TTL          int
	AliveTimer   int
	LogLevel     string
	CipherWindow int
}

var instance *Config
var once sync.Once

// Config singleton
func GetConfig() (*Config, error) {
	var err error
	once.Do(func() {
		if instance == nil {
			instance, err = parseConfigFile()
			// TODO: force error to caller
			// WARNING: once.Do doesn´t support return here
			if err != nil {
				panic(err)
			}
		}
	})
	return instance, nil
}

// Same as GetConfig but without error
// Used only to get AliveTimer without testing
func MustGetConfig() *Config {
	config, err := GetConfig()
	if err != nil {
		panic(err)
	}
	return config
}

// GetConfigDir returns the config directory
// WARNING: this probably won't work on MacOS
func GetConfigDir() string {
	if runtime.GOOS == "windows" {
		return os.Getenv("USERPROFILE") + "\\AppData\\Local\\xAAL\\"
	}
	return os.Getenv("HOME") + "/.xaal/"
}

func parseConfigFile() (*Config, error) {
	// setting default values
	config := &Config{
		Address:      defaultAddr,
		Port:         defaultPort,
		BindAddress:  defaultBindAddr,
		TTL:          defaultTTL,
		AliveTimer:   defaultAliveTimer,
		LogLevel:     defaultLogLevel,
		CipherWindow: defaultCipherWindow,
	}
	// load config file
	cfg, err := ini.Load(GetConfigDir() + "xaal.ini")
	if err != nil {
		return nil, err
	}
	// loading values
	sec := cfg.Section(ini.DefaultSection)
	items := make(map[string]interface{})
	items["key"] = &config.KeyString
	items["address"] = &config.Address
	items["bind_address"] = &config.BindAddress
	items["port"] = &config.Port
	items["hops"] = &config.TTL
	items["alive_timer"] = &config.AliveTimer
	items["log_level"] = &config.LogLevel
	items["cipher_window"] = &config.CipherWindow
	for key, value := range items {
		err := setConfigItem(sec, key, value)
		if err != nil {
			return nil, err
		}
	}
	// Key is mandatory
	if config.KeyString == "" {
		return nil, fmt.Errorf("CONFIG ERROR: key is empty")
	}
	return config, nil
}

func setConfigItem(sec *ini.Section, key string, target interface{}) error {
	if sec.HasKey(key) {
		value := sec.Key(key).Value()
		switch v := target.(type) {
		case *string:
			if value == "" {
				return fmt.Errorf("CONFIG ERROR: key %s is empty", key)
			}
			*v = value
		case *int:
			var err error
			*v, err = strconv.Atoi(value)
			if err != nil {
				return fmt.Errorf("CONFIG ERROR: key %s is not an integer", key)
			}
		default:
			return fmt.Errorf("CONFIG ERROR: Unsupported target type for key %s", key)
		}
	}
	return nil
}
