package xaal

import (
	"fmt"
	"log/slog"
	"net"

	"golang.org/x/sys/unix"
)

const maxUDPPayload = 65507

type NetworkConnector struct {
	Addr  unix.SockaddrInet4
	Sock  int
	queue *Queue
}

// NewNetworkConnector creates a new instance of NetworkConnector.
func NewNetworkConnector(addr string, port int, bindAddr string, ttl int) (*NetworkConnector, error) {
	queue := NewQueue()

	nc := &NetworkConnector{queue: queue}

	// Configure the address to receive multicast traffic on the specified interface
	conn, err := unix.Socket(unix.AF_INET, unix.SOCK_DGRAM, 0)
	if err != nil {
		return nil, fmt.Errorf("SOCK ERROR: %v", err)
	}
	nc.Sock = conn

	// Enable SO_REUSEPORT
	err = unix.SetsockoptInt(nc.Sock, unix.SOL_SOCKET, unix.SO_REUSEPORT, 1)
	if err != nil {
		unix.Close(nc.Sock)
		return nil, fmt.Errorf("SO_REUSEPORT ERROR: %v", err)
	}

	// bind the socket to the specified bind address, this is mandatory on BSD
	var bindIP [4]byte
	copy(bindIP[:], net.ParseIP(bindAddr).To4())
	bindInet := unix.SockaddrInet4{Port: port, Addr: bindIP}
	if err := unix.Bind(nc.Sock, &bindInet); err != nil {
		unix.Close(nc.Sock)
		return nil, fmt.Errorf("BIND ERROR: %v", err)
	}

	// Subscribe to the multicast group
	var mcastIP [4]byte
	copy(mcastIP[:], net.ParseIP(addr).To4())
	nc.Addr = unix.SockaddrInet4{Port: port, Addr: mcastIP}
	mreq := &unix.IPMreq{
		Multiaddr: nc.Addr.Addr,
		Interface: bindIP, // Use the binded interface
	}
	if err := unix.SetsockoptIPMreq(nc.Sock, unix.IPPROTO_IP, unix.IP_ADD_MEMBERSHIP, mreq); err != nil {
		unix.Close(nc.Sock)
		return nil, fmt.Errorf("IP_ADD_MEMBERSHIP ERROR: %v", err)
	}

	// Set the TTL for multicast packets
	if err := unix.SetsockoptInt(nc.Sock, unix.IPPROTO_IP, unix.IP_MULTICAST_TTL, ttl); err != nil {
		unix.Close(nc.Sock)
		return nil, fmt.Errorf("IP_MULTICAST_TTL ERROR: %v", err)
	}
	slog.Debug("Connected", "addr", addr, "port", port, "bindAddr", bindAddr, "ttl", ttl)
	return nc, nil
}

// Send sends a message through the network.
// Error is logged to like in Receive.
func (nc *NetworkConnector) Send(buf []byte) {
	err := unix.Sendto(nc.Sock, buf, 0, &nc.Addr)
	if err != nil {
		slog.Error("Error sending message", "err", err)
	}
}

// Receive listens for incoming network messages and processes them.
// Error is logged to avoid to handle it in goroutine.
func (nc *NetworkConnector) Receive() {
	for {
		buffer := make([]byte, maxUDPPayload)
		n, _, err := unix.Recvfrom(nc.Sock, buffer, 0)
		if err != nil {
			slog.Error("Error receving message", "err", err)
		} else {
			nc.queue.Push(buffer[:n])
		}
	}
}

func (nc *NetworkConnector) Stop() {
	unix.Close(nc.Sock)
}
