package xaal

import (
	"log/slog"
	"os"

	"github.com/MatusOllah/slogcolor"
	"github.com/mattn/go-isatty"
)

func SetupLogger(level slog.Level) {
	// level := "DEBUG"
	// w := os.Stderr
	// logger := slog.New(
	// 	tint.NewHandler(w, &tint.Options{
	// 		NoColor:    !isatty.IsTerminal(w.Fd()),
	// 		TimeFormat: "2006/01/02 15:04:05",
	// 		Level:      slog.LevelDebug,
	// 		// AddSource:  true,
	// 	}),
	// )
	// slog.SetDefault(logger)

	w := os.Stderr
	opts := &slogcolor.Options{
		Level:       level,
		SrcFileMode: slogcolor.ShortFile,
		TimeFormat:  "2006/01/02 15:04:05",
		NoColor:     !isatty.IsTerminal(w.Fd()),
	}
	slog.SetDefault(slog.New(slogcolor.NewHandler(w, opts)))
}
