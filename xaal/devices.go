package xaal

import (
	"bytes"
	"fmt"
	"log/slog"
	"math/rand"
	"reflect"
	"strings"
	"time"

	"github.com/fxamacker/cbor/v2"
	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
)

type Attribute struct {
	Value  interface{}
	Device *Device
	Name   string
}

type Device struct {
	Engine                   *Engine
	NextAlive                time.Time
	Methods                  map[string]interface{}
	Attributes               map[string]*Attribute
	URL                      string
	VendorID                 string
	ProductID                string
	HWID                     string
	Version                  string
	Schema                   string
	Info                     string
	DevType                  string
	UnsupportedAttributes    []string
	UnsupportedMethods       []string
	UnsupportedNotifications []string
	GroupID                  uuid.UUID
	Address                  uuid.UUID
}

func NewDevice(devType string) *Device {
	dev := &Device{DevType: devType}
	dev.Attributes = make(map[string]*Attribute)
	dev.Methods = make(map[string]interface{})
	dev.AddMethod(string(MSGActionGetAttributes), dev.getAttributes)
	dev.AddMethod(string(MSGActionGetDescription), dev.getDescription)
	return dev
}

// Return a formated string representation of the device
func (dev *Device) ToDump() string {
	tab := table.NewWriter()
	tab.SetTitle("Dev:" + dev.Address.String())
	tab.SetStyle(table.StyleRounded)
	tab.AppendRow(table.Row{"Address", dev.Address})
	tab.AppendRow(table.Row{"DevType", dev.DevType})
	// description
	descBody := dev.getDescription(nil)
	if len(*descBody) > 0 {
		desc := interfaceToString(*descBody)
		tab.AppendRow(table.Row{"Description", desc})
	}
	// attributes
	if len(dev.Attributes) > 0 {
		attrs := interfaceToString(dev.Attributes)
		tab.AppendRow(table.Row{"Attributes", attrs})
	}
	// methods
	if len(dev.Methods) > 0 {
		methods := interfaceToString(dev.Methods)
		tab.AppendRow(table.Row{"Methods", methods})
	}
	return tab.Render()
}

// display a device
func (dev *Device) Dump() {
	fmt.Println(dev.ToDump())
}

// return a string representation of the device
func (dev *Device) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "<dev %p %s %s >", dev, dev.Address, dev.DevType)
	return buf.String()
}

// Return true if this device should emit an alive message
func (dev *Device) ShouldEmitAlive() bool {
	return time.Now().After(dev.NextAlive)
}

// Update the next alive message time. This function add a random
// number to the interval, to avoid bulk sending.
func (dev *Device) UpdateNextAlive(interval int) {
	value := time.Duration(interval+rand.Intn(5)) * time.Second
	dev.NextAlive = time.Now().Add(value)
}

// =============================================================================
// Attributes
// =============================================================================

func (attr *Attribute) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "<Attribute %p %s(%T): %v>", attr, attr.Name, attr.Value, attr.Value)
	return buf.String()
}

// Update the attribute value with type check
func (attr *Attribute) SetValue(value interface{}) {
	if attr.Value == nil {
		attr.Value = value
	} else {
		prevType := reflect.TypeOf(attr.Value)
		newType := reflect.TypeOf(value)
		if newType != prevType {
			slog.Warn("Type mismatch", "prevType", prevType, "newType", newType, "attr", attr)
			return
		}

		if !reflect.DeepEqual(value, attr.Value) {
			attr.Value = value
			if attr.Device.Engine != nil {
				attr.Device.Engine.QueueAttributeChange(attr)
			}
		}
	}
}

func (dev *Device) AddAttribute(name string, value interface{}) *Attribute {
	attrib := &Attribute{Device: dev, Name: name, Value: value}
	dev.Attributes[name] = attrib
	return attrib
}

func (dev *Device) RemoveAttribute(name string) {
	if attr, exists := dev.Attributes[name]; exists {
		attr.Device = nil
		delete(dev.Attributes, name)
	}
}

func (dev *Device) GetAttribute(name string) *Attribute {
	return dev.Attributes[name]
}

// =============================================================================
// Methods
// =============================================================================

func (dev *Device) AddMethod(name string, fn interface{}) {
	dev.Methods[name] = fn
}

func (dev *Device) RemoveMethod(name string) {
	delete(dev.Methods, name)
}

func (dev *Device) GetMethod(name string) interface{} {
	return dev.Methods[name]
}

func (dev *Device) SetMethod(name string, fn interface{}) {
	dev.Methods[name] = fn
}

// =============================================================================
// Default methods
// =============================================================================

func (dev *Device) getAttributes(MessageBody) *MessageBody {
	b := make(MessageBody)
	for _, a := range dev.Attributes {
		b[a.Name] = a.Value
	}
	return &b
}

func (dev *Device) getDescription(MessageBody) *MessageBody {
	body := make(MessageBody)
	if dev.GroupID != uuid.Nil {
		body["group_id"] = cbor.Tag{Number: 37, Content: dev.GroupID}
	}
	if dev.VendorID != "" {
		body["vendor_id"] = dev.VendorID
	}
	if dev.ProductID != "" {
		body["product_id"] = dev.ProductID
	}
	if dev.HWID != "" {
		body["hwid"] = dev.HWID
	}
	if dev.Version != "" {
		body["version"] = dev.Version
	}
	if dev.URL != "" {
		body["url"] = cbor.Tag{Number: 32, Content: dev.URL}
	}
	if dev.Schema != "" {
		body["schema"] = cbor.Tag{Number: 32, Content: dev.Schema}
	}
	if dev.Info != "" {
		body["info"] = dev.Info
	}
	if len(dev.UnsupportedAttributes) > 0 {
		body["unsupported_attributes"] = dev.UnsupportedAttributes
	}
	if len(dev.UnsupportedMethods) > 0 {
		body["unsupported_methods"] = dev.UnsupportedMethods
	}
	if len(dev.UnsupportedNotifications) > 0 {
		body["unsupported_notifications"] = dev.UnsupportedNotifications
	}
	return &body
}

// =============================================================================
// Helpers
// =============================================================================

// check is the device matches the given devType
func (dev *Device) MatchDevTypes(devTypes []string) bool {
	any := strings.Split(dev.DevType, ".")[0]

	for _, devType := range devTypes {
		// any.any
		if devType == "any.any" {
			return true
		}
		// my dev_type
		if devType == dev.DevType {
			return true
		}
		// my dev_type.any
		target := strings.Split(devType, ".")
		if (target[1] == "any") && (target[0] == any) {
			return true
		}
	}
	return false
}

// forward a notification to the engine
func (dev *Device) SendNotification(notification string, body MessageBody) {
	dev.Engine.SendNotification(dev, notification, body)
}
