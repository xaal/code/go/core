package xaal

import (
	"container/list"
	"fmt"
	"reflect"
	"strings"
	"sync"

	"github.com/fxamacker/cbor/v2"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
)

type Queue struct {
	list *list.List
	mut  sync.Mutex
}

func NewQueue() *Queue {
	return &Queue{
		list: list.New(),
	}
}

func (q *Queue) Push(v interface{}) {
	q.mut.Lock()
	defer q.mut.Unlock()
	q.list.PushBack(v)
}

func (q *Queue) Pop() interface{} {
	q.mut.Lock()
	defer q.mut.Unlock()
	elt := q.list.Front()
	if elt != nil {
		r := elt.Value
		q.list.Remove(elt)
		return r
	}
	return nil
}

func (q *Queue) Flush() []interface{} {
	q.mut.Lock()
	defer q.mut.Unlock()
	r := []interface{}{}
	for elt := q.list.Front(); elt != nil; elt = elt.Next() {
		r = append(r, elt.Value)
	}
	q.list.Init()
	return r
}

func (q *Queue) Len() int {
	q.mut.Lock()
	defer q.mut.Unlock()
	return q.list.Len()
}

// Transform map[interface{}]interface{} to map[string]interface{}
// cbor lib return a map[interface{}]interface{}, and we need a map[string]interface{}
// used mainly for msg.Body
func decodeMap(originalMap map[interface{}]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for key, value := range originalMap {
		if stringKey, ok := key.(string); ok {
			result[stringKey] = value
		}
	}
	return result
}

// Transform interface{} to []string
func decodeSlice(value interface{}) []string {
	var result []string
	originalSlice := value.([]interface{})
	for _, value := range originalSlice {
		result = append(result, value.(string))
	}
	return result
}

// Convert value to the specified type
func convertToType(value interface{}, targetType reflect.Type) interface{} {
	// Attempt type conversion
	valueReflect := reflect.ValueOf(value)
	if valueReflect.Type().ConvertibleTo(targetType) {
		return valueReflect.Convert(targetType).Interface()
	}
	return nil
}

// Convert any cbor tag to a string
func cborTagToString(tag cbor.Tag) string {
	if tag.Number == 37 {
		var value uuid.UUID
		switch content := tag.Content.(type) {
		case uuid.UUID:
			value = content
		case []byte:
			var err error
			value, err = uuid.FromBytes(content)
			// value = tag.Content.(uuid.UUID)
			if err != nil {
				return fmt.Sprintf("Err UUID from bytes: %v", tag.Content)
			}
		default:
			return fmt.Sprintf("Invalid UUID: %v", reflect.TypeOf(tag.Content))
		}
		return fmt.Sprintf("%s (UUID)", value)
	}
	if tag.Number == 32 {
		return fmt.Sprintf("%s (URL)", tag.Content)
	}
	return fmt.Sprintf("%v (cborTag:%d)", tag.Number, tag.Content)
}

// Convert anything to a display list
func convertInterfaceToString(value interface{}, indent string) string {
	var builder strings.Builder
	v := reflect.ValueOf(value)
	t := reflect.TypeOf(value)

	switch v.Kind() {

	case reflect.Slice, reflect.Array:
		builder.WriteString(fmt.Sprintf("%s\n", indent))
		for i := 0; i < v.Len(); i++ {
			builder.WriteString(fmt.Sprintf("%s- ", indent))
			builder.WriteString(convertInterfaceToString(v.Index(i).Interface(), indent+"  "))
		}

	case reflect.Map:
		builder.WriteString(fmt.Sprintf("%s\n", indent))
		for _, key := range v.MapKeys() {
			builder.WriteString(fmt.Sprintf("%s%v: ", indent, key))
			builder.WriteString(convertInterfaceToString(v.MapIndex(key).Interface(), indent+"  "))
		}

	default:
		// Display final element
		tagT := reflect.TypeOf(cbor.Tag{})
		if reflect.TypeOf(value) == tagT {
			builder.WriteString(cborTagToString(value.(cbor.Tag)) + "\n")
			break
		} else {
			if value != nil {
				builder.WriteString(fmt.Sprintf("%v (%s)\n", v.Interface(), t))
			} else {
				builder.WriteString("nil\n")
			}
		}
	}

	result := builder.String()
	return result
}

func interfaceToString(value interface{}) string {
	// call the recursive function and remove the first and last newline
	result := convertInterfaceToString(value, "")
	result = strings.TrimPrefix(result, "\n")
	result = strings.TrimSuffix(result, "\n")
	return result
}
