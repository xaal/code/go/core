package xaal

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"time"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"

	"github.com/fatih/color"
	"github.com/fxamacker/cbor/v2"
	"github.com/jedib0t/go-pretty/v6/table"
	"golang.org/x/crypto/chacha20poly1305"
)

const (
	MSGTypeNotify  MessageType = 0
	MSGTypeRequest MessageType = 1
	MGSTypeReply   MessageType = 2

	MSGActionAlive            MessageAction = "alive"
	MSGActionIsAlive          MessageAction = "is_alive"
	MSGActionAttributesChange MessageAction = "attributes_change"
	MSGActionGetAttributes    MessageAction = "get_attributes"
	MSGActionGetDescription   MessageAction = "get_description"
)

type (
	MessageType   uint64
	MessageAction string
	MessageBody   map[string]interface{}
)

type MessageFactory struct {
	key    []byte
	filter func(*Message) bool
}

type Message struct {
	Body      MessageBody
	DevType   string
	Action    string
	Timestamp []uint64
	Targets   []uuid.UUID
	Version   int
	Type      MessageType
	Source    uuid.UUID
}

// =============================================================================
// MessageFactory
// =============================================================================

// Create a new MessageFactory, ready to encode/decode messages
// It return an error if the key is invalid
func NewMessageFactory(keyString string) (*MessageFactory, error) {
	key, err := hex.DecodeString(keyString)
	if err != nil {
		return nil, fmt.Errorf("KEY ERROR: %s", err)
	}
	// Try to load the key, to avoid to fail at runtime
	_, err = chacha20poly1305.New(key)
	if err != nil {
		return nil, fmt.Errorf("LOAD KEY ERROR: %s", err)
	}

	mf := &MessageFactory{
		key: key,
	}
	return mf, nil
}

// Decode/decipher incomming message
// return a Message or error if decoding is not possible
func (mf *MessageFactory) DecodeMSG(pkt []byte) (*Message, error) {
	var data []interface{}
	var targets []interface{}
	var payload []interface{}

	err := cbor.Unmarshal(pkt, &data)
	if err != nil {
		return nil, fmt.Errorf("PKT UNMARSHAL ERROR: %s", err)
	}

	// unmarshal the security layer
	msg := NewMessage()
	if len(data) < 4 {
		return nil, fmt.Errorf("MSG TOO SHORT: %d", len(data))
	}

	msg.Version = int(data[0].(uint64))
	if msg.Version != 7 {
		return nil, fmt.Errorf("VERSION ERROR: %d", msg.Version)
	}
	msg.Timestamp[0] = data[1].(uint64)
	msg.Timestamp[1] = data[2].(uint64)
	err = cbor.Unmarshal(data[3].([]byte), &targets)
	if err != nil {
		return nil, fmt.Errorf("TARGETS UNMARSHAL ERROR: %s", err)
	}
	// replay attack prevention
	now := time.Now().Unix()
	msgTime := int64(msg.Timestamp[0])
	cipherWindow := int64(MustGetConfig().CipherWindow)
	if msgTime < (now - cipherWindow) {
		return nil, fmt.Errorf("TIMESTAMP ERROR: MSG too old")
	}
	if msgTime > (now + cipherWindow) {
		return nil, fmt.Errorf("TIMESTAMP ERROR: MSG too young")
	}
	// targets
	for k := range targets {
		tmpTarget, _ := uuid.FromBytes(targets[k].([]byte))
		msg.Targets = append(msg.Targets, tmpTarget)
	}
	// filter the message if not for us
	if mf.filter(msg) == false {
		return nil, nil
	}

	// prepare the deciphering
	cipher := data[4].([]byte)
	ad := data[3].([]byte)
	nonce := buildNonce(msg.Timestamp)

	// decipher the message
	block, _ := chacha20poly1305.New(mf.key)
	clear, err := block.Open(nil, nonce, cipher, ad)
	if err != nil {
		return nil, fmt.Errorf("MSG DECIPHER ERROR: %s", err)
	}

	// unmarshal the payload
	err = cbor.Unmarshal(clear, &payload)
	if err != nil {
		return nil, fmt.Errorf("PAYLOAD UNMARSHAL ERROR: %s", err)
	}
	// let's check every fields
	src := payload[0].([]byte)
	// msg.Source
	msg.Source, err = uuid.FromBytes(src)
	if err != nil {
		return nil, fmt.Errorf("UUID ERROR: %s", err)
	}
	// msg.Devtype
	if devType, ok := payload[1].(string); ok {
		msg.DevType = devType
	} else {
		return nil, fmt.Errorf("DEVTYPE ERROR: NOT A STRING")
	}
	// msg.Type
	if msgType, ok := payload[2].(uint64); ok {
		msg.Type = MessageType(msgType)
	} else {
		return nil, fmt.Errorf("MSGTYPE ERROR: NOT A UINT64")
	}
	// msg.Action
	if action, ok := payload[3].(string); ok {
		msg.Action = action
	} else {
		return nil, fmt.Errorf("ACTION ERROR: NOT A STRING")
	}
	// msg.Body
	if len(payload) == 5 {
		if body, ok := payload[4].(map[interface{}]interface{}); ok {
			msg.Body = MessageBody(decodeMap(body))
		} else {
			return nil, fmt.Errorf("PAYLOAD BODY ERROR: NOT A MAP")
		}
	}
	return msg, nil
}

// Encode/cipher a Message
// return a ciphered byte array or an error if encoding is not possible
func (mf *MessageFactory) EncodeMSG(msg *Message) ([]byte, error) {
	var err error

	// security layer
	result := make([]interface{}, 5)
	result[0] = msg.Version
	result[1] = msg.Timestamp[0]
	result[2] = msg.Timestamp[1]
	result[3], err = cbor.Marshal(msg.Targets)
	if err != nil {
		return nil, fmt.Errorf("TARGETS MARSHAL ERROR: %s", err)
	}

	// payload
	buf := make([]interface{}, 5)
	buf[0] = msg.Source
	buf[1] = msg.DevType
	buf[2] = msg.Type.EnumIndex()
	buf[3] = msg.Action
	buf[4] = msg.Body
	clear, err := cbor.Marshal(buf)
	if err != nil {
		return nil, fmt.Errorf("PAYLOAD MARSHAL ERROR: %s", err)
	}

	// ciphering
	ad := result[3].([]byte)
	nonce := buildNonce(msg.Timestamp)
	block, _ := chacha20poly1305.New(mf.key)
	payload := block.Seal(nil, nonce, clear, ad)

	// pack everything
	result[4] = payload
	pkt, err := cbor.Marshal(result)
	if err != nil {
		return nil, fmt.Errorf("PKT MARSHAL ERROR: %s", err)
	}
	return pkt, nil
}

// Build a Message and set values
func (mf *MessageFactory) BuildMessage(dev *Device, targets []uuid.UUID, msgType MessageType, action string, body MessageBody) *Message {
	msg := NewMessage()
	msg.Source = dev.Address
	msg.DevType = dev.DevType
	msg.Targets = targets
	msg.Type = msgType
	msg.Action = action
	msg.Body = body
	return msg
}

func (mf *MessageFactory) SetFilter(filter func(*Message) bool) {
	mf.filter = filter
}

// =============================================================================
// Messages
// =============================================================================

// Return a new empty Message with version & timestamp initialized
func NewMessage() *Message {
	msg := &Message{Version: 7}
	msg.Timestamp = make([]uint64, 2)
	return msg
}

// Returns a string (table) representation of a Message
func (msg *Message) ToTab() string {
	tab := table.NewWriter()
	tab.SetStyle(table.StyleRounded)
	tab.AppendRow(table.Row{"Version", msg.Version})
	tab.AppendRow(table.Row{"Timestamp", msg.Timestamp})
	tab.AppendRow(table.Row{"Targets", msg.Targets})
	tab.AppendRow(table.Row{"Source", msg.Source})
	tab.AppendRow(table.Row{"DevType", msg.DevType})
	tab.AppendRow(table.Row{"Type", msg.Type})
	tab.AppendRow(table.Row{"Action", msg.Action})
	if msg.Body != nil {
		body := interfaceToString(msg.Body)
		tab.AppendRow(table.Row{"Body", body})
	}
	return tab.Render()
}

// Print a Message to the console w/ colors
func (msg *Message) Dump() {
	colorFunc := color.YellowString
	if msg.IsAlive() {
		colorFunc = color.WhiteString
	} else if msg.IsNotify() {
		colorFunc = color.RedString
	} else if msg.IsRequest() {
		colorFunc = color.GreenString
	} else if msg.IsReply() {
		colorFunc = color.CyanString
	}
	fmt.Println(colorFunc(msg.ToTab()))
}

// Return true if the message is a Notify
func (msg *Message) IsNotify() bool {
	return msg.Type == MSGTypeNotify
}

// Return true if the message is a Request
func (msg *Message) IsRequest() bool {
	return msg.Type == MSGTypeRequest
}

// Return true if the message is a Reply
func (msg *Message) IsReply() bool {
	return msg.Type == MGSTypeReply
}

// Return true if the message is a Request or a Reply
func (msg *Message) IsRequestOrReply() bool {
	return msg.Type == MSGTypeRequest || msg.Type == MGSTypeReply
}

// Return true if the message is a Alive notification
func (msg *Message) IsAlive() bool {
	if msg.Type == MSGTypeNotify && msg.Action == string(MSGActionAlive) {
		return true
	}
	return false
}

// Retunr true if the message is a IsAlive request
func (msg *Message) IsIsAlive() bool {
	if msg.Type == MSGTypeRequest && msg.Action == string(MSGActionIsAlive) {
		return true
	}
	return false
}

// Return true if the message is a AttributesChange notification
func (msg *Message) IsAttributesChange() bool {
	if msg.Type == MSGTypeNotify && msg.Action == string(MSGActionAttributesChange) {
		return true
	}
	return false
}

// Return true if the message is a GetAttributes request
func (msg *Message) IsGetAttributes() bool {
	if msg.IsRequestOrReply() && msg.Action == string(MSGActionGetAttributes) {
		return true
	}
	return false
}

// Return true if the message is a GetDescription request
func (msg *Message) IsGetDescription() bool {
	if msg.IsRequestOrReply() && msg.Action == string(MSGActionGetDescription) {
		return true
	}
	return false
}

// String representation of a MessageType
func (t MessageType) String() string {
	return [...]string{"NOTIFY", "REQUEST", "REPLY"}[t]
}

// Return the enum index of a MessageType
func (t MessageType) EnumIndex() int {
	return int(t)
}

// Build a nonce (12 bytes long) w/ a given timestamp struct
func buildNonce(data []uint64) []byte {
	// Big-Endian, time in seconds (64 bits) and microseconds (32 bits)
	nonce := make([]byte, 12)
	binary.BigEndian.PutUint64(nonce[:8], data[0])
	binary.BigEndian.PutUint32(nonce[8:], uint32(data[1]))
	return nonce
}
