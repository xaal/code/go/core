package schemas

import (
	"log/slog"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

// =====================================================================
func NewBarometer(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("barometer.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Atmospheric pressure | type: data = number | unit: mbar
	dev.AddAttribute("pressure", 0.0)

	return dev
}

// =====================================================================
func NewBasic(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("basic.basic")
	dev.Address = addr

	return dev
}

// =====================================================================
func NewBattery(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("battery.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Battery energy level | type: data = (0..100) / (0.0..100.0) | unit: %EL
	dev.AddAttribute("level", 0.0)
	// List of devices concerned with this battery | type: data = [ * #6.37(bstr .size 16) ]
	dev.AddAttribute("devices", nil)

	return dev
}

// =====================================================================
func NewButton(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("button.basic")
	dev.Address = addr

	return dev
}

// =====================================================================
func NewButtonRemote(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("button.remote")
	dev.Address = addr

	// -------- Methods --------
	// Return the list of buttons
	defaultGetButtons := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultGetButtons method: get_buttons")
		return nil
	}
	dev.AddMethod("get_buttons", defaultGetButtons)

	return dev
}

// =====================================================================
func NewCache(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("cache.basic")
	dev.Address = addr

	// -------- Methods --------
	// Get attributes of a device in cache with last values and dates
	defaultGetDeviceAttributes := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: device, attributes
		slog.Info("defaultGetDeviceAttributes method: get_device_attributes", "args", args)
		return nil
	}
	dev.AddMethod("get_device_attributes", defaultGetDeviceAttributes)

	return dev
}

// =====================================================================
func NewCo2Meter(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("co2meter.basic")
	dev.Address = addr

	// -------- Attributes --------
	// CO2 | type: data = uint | unit: ppm
	dev.AddAttribute("co2", 0)

	return dev
}

// =====================================================================
func NewContact(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("contact.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Detection status of the contact sensor | type: data = bool
	dev.AddAttribute("detected", false)

	return dev
}

// =====================================================================
func NewDoor(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("door.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Position of the door (true=open false=close) | type: data = bool
	dev.AddAttribute("position", false)

	// -------- Methods --------
	// Open the door
	defaultOpen := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultOpen method: open")
		return nil
	}
	// Close the door
	defaultClose := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultClose method: close")
		return nil
	}
	dev.AddMethod("open", defaultOpen)
	dev.AddMethod("close", defaultClose)

	return dev
}

// =====================================================================
func NewFalldetector(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("falldetector.basic")
	dev.Address = addr

	// -------- Attributes --------
	// List of detected falls - List of objects {delay,zone,x,y} whose semantic is device-dependent; Each object means a fall; An empty object means a detecting fall with no additional info; An empty list means no detected fall. | type: data = [ * { ?"delay":#6.1(uint), ?"zone":tstr, ?"x":number, ?"y":number } ]
	dev.AddAttribute("falls", nil)

	return dev
}

// =====================================================================
func NewGateway(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("gateway.basic")
	dev.Address = addr

	// -------- Attributes --------
	// List of embeded devices | type: data = [ * #6.37(bstr .size 16) ]
	dev.AddAttribute("embedded", nil)
	// List of embeded devices | type: data = [ * #6.37(bstr .size 16) ]
	dev.AddAttribute("inactive", nil)

	return dev
}

// =====================================================================
func NewHmi(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("hmi.basic")
	dev.Address = addr

	return dev
}

// =====================================================================
func NewHygrometer(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("hygrometer.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Humidity | type: data = (0..100) / (0.0..100.0) | unit: %RH
	dev.AddAttribute("humidity", 0.0)

	return dev
}

// =====================================================================
func NewLamp(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("lamp.basic")
	dev.Address = addr

	// -------- Attributes --------
	// State of the lamp (true=on false=off) | type: data = bool
	dev.AddAttribute("light", false)

	// -------- Methods --------
	// Switch on the lamp
	defaultTurnOn := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOn method: turn_on")
		return nil
	}
	// Switch off the lamp
	defaultTurnOff := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOff method: turn_off")
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)

	return dev
}

// =====================================================================
func NewLampColor(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("lamp.color")
	dev.Address = addr

	// -------- Attributes --------
	// State of the lamp (true=on false=off) | type: data = bool
	dev.AddAttribute("light", false)
	// Level of the brightness | type: data = (0..100) / (0.0..100.0) | unit: %
	dev.AddAttribute("brightness", 0.0)
	// Temperature of the white | type: data = 1000..10000 | unit: K
	dev.AddAttribute("white_temperature", 0)
	// Color description in hue-saturation-value coding | type: data = [ ((0..360)/(0.0..360.0)),  (0.0..1.0),  (0.0..1.0) ]
	dev.AddAttribute("hsv", nil)
	// Mode of the lamp | type: data = "white" / "color" / "scene"
	dev.AddAttribute("mode", nil)
	// Scene name | type: data = tstr
	dev.AddAttribute("scene", nil)

	// -------- Methods --------
	// Switch on the lamp
	defaultTurnOn := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: smooth
		slog.Info("defaultTurnOn method: turn_on", "args", args)
		return nil
	}
	// Switch off the lamp
	defaultTurnOff := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: smooth
		slog.Info("defaultTurnOff method: turn_off", "args", args)
		return nil
	}
	// Change the brightness of the lamp
	defaultSetBrightness := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: brightness, smooth
		slog.Info("defaultSetBrightness method: set_brightness", "args", args)
		return nil
	}
	// Change the temperature of the white
	defaultSetWhiteTemperature := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: white_temperature
		slog.Info("defaultSetWhiteTemperature method: set_white_temperature", "args", args)
		return nil
	}
	// Change the color of the lamp
	defaultSetHsv := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: hsv, smooth
		slog.Info("defaultSetHsv method: set_hsv", "args", args)
		return nil
	}
	// Change the mode of the lamp
	defaultSetMode := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: mode
		slog.Info("defaultSetMode method: set_mode", "args", args)
		return nil
	}
	// Change the scene played by the color-changing lamp
	defaultSetScene := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: scene, options
		slog.Info("defaultSetScene method: set_scene", "args", args)
		return nil
	}
	// Get scenes names supported by the lamp
	defaultGetScenes := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultGetScenes method: get_scenes")
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)
	dev.AddMethod("set_brightness", defaultSetBrightness)
	dev.AddMethod("set_white_temperature", defaultSetWhiteTemperature)
	dev.AddMethod("set_hsv", defaultSetHsv)
	dev.AddMethod("set_mode", defaultSetMode)
	dev.AddMethod("set_scene", defaultSetScene)
	dev.AddMethod("get_scenes", defaultGetScenes)

	return dev
}

// =====================================================================
func NewLampDimmer(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("lamp.dimmer")
	dev.Address = addr

	// -------- Attributes --------
	// State of the lamp (true=on false=off) | type: data = bool
	dev.AddAttribute("light", false)
	// Level of the brightness | type: data = (0..100) / (0.0..100.0) | unit: %
	dev.AddAttribute("brightness", 0.0)
	// Temperature of the white | type: data = 1000..10000 | unit: K
	dev.AddAttribute("white_temperature", 0)

	// -------- Methods --------
	// Switch on the lamp
	defaultTurnOn := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: smooth
		slog.Info("defaultTurnOn method: turn_on", "args", args)
		return nil
	}
	// Switch off the lamp
	defaultTurnOff := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: smooth
		slog.Info("defaultTurnOff method: turn_off", "args", args)
		return nil
	}
	// Change the brightness of the lamp
	defaultSetBrightness := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: brightness, smooth
		slog.Info("defaultSetBrightness method: set_brightness", "args", args)
		return nil
	}
	// Change the temperature of the white
	defaultSetWhiteTemperature := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: white_temperature
		slog.Info("defaultSetWhiteTemperature method: set_white_temperature", "args", args)
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)
	dev.AddMethod("set_brightness", defaultSetBrightness)
	dev.AddMethod("set_white_temperature", defaultSetWhiteTemperature)

	return dev
}

// =====================================================================
func NewLampToggle(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("lamp.toggle")
	dev.Address = addr

	// -------- Attributes --------
	// State of the lamp (true=on false=off) | type: data = bool
	dev.AddAttribute("light", false)

	// -------- Methods --------
	// Switch on the lamp
	defaultTurnOn := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOn method: turn_on")
		return nil
	}
	// Switch off the lamp
	defaultTurnOff := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOff method: turn_off")
		return nil
	}
	// Toggle lamp state
	defaultToggle := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultToggle method: toggle")
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)
	dev.AddMethod("toggle", defaultToggle)

	return dev
}

// =====================================================================
func NewLightgauge(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("lightgauge.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Brightness indicator without units (scaled in 100 levels of brightness; Information is transmitted in percentage). Simple LDR (photoresistor). | type: data = (0..100) / (0.0..100.0) | unit: %
	dev.AddAttribute("brightness", 0.0)

	return dev
}

// =====================================================================
func NewLinkquality(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("linkquality.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Link quality | type: data = (0..100) / (0.0..100.0) | unit: %
	dev.AddAttribute("level", 0.0)
	// List of devices concerned with this link | type: data = [ * #6.37(bstr .size 16) ]
	dev.AddAttribute("devices", nil)

	return dev
}

// =====================================================================
func NewLuxmeter(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("luxmeter.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Lux | type: data = number | unit: lx
	dev.AddAttribute("illuminance", 0.0)

	return dev
}

// =====================================================================
func NewMetadatadb(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("metadatadb.basic")
	dev.Address = addr

	// -------- Methods --------
	// Get the list of known devices; possibly filtered by a key, a (non-null) value, or both
	defaultGetDevices := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: key, value
		slog.Info("defaultGetDevices method: get_devices", "args", args)
		return nil
	}
	// Get the map of keys-values associated with a given device; possibly restricted to a list of wanted keys
	defaultGetKeysValues := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: device, keys
		slog.Info("defaultGetKeysValues method: get_keys_values", "args", args)
		return nil
	}
	// Get the value of a key of a device
	defaultGetValue := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: device, key
		slog.Info("defaultGetValue method: get_value", "args", args)
		return nil
	}
	// Update keys-values on a device; non-existing keys are added; the null value deletes a key; a null map deletes all keys-values of a device; devices with no more key-value are withdrawn of the DB
	defaultUpdateKeysValues := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: device, map
		slog.Info("defaultUpdateKeysValues method: update_keys_values", "args", args)
		return nil
	}
	dev.AddMethod("get_devices", defaultGetDevices)
	dev.AddMethod("get_keys_values", defaultGetKeysValues)
	dev.AddMethod("get_value", defaultGetValue)
	dev.AddMethod("update_keys_values", defaultUpdateKeysValues)

	return dev
}

// =====================================================================
func NewMotion(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("motion.basic")
	dev.Address = addr

	// -------- Attributes --------
	// State of the presence (true=yes false=no) | type: data = bool
	dev.AddAttribute("presence", false)

	return dev
}

// =====================================================================
func NewPowermeter(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("powermeter.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Current power | type: data = number | unit: W
	dev.AddAttribute("power", 0.0)
	// Energy used or produced since cycle beginning | type: data = number | unit: kWh
	dev.AddAttribute("energy", 0.0)
	// List of devices concerned with this powermeter | type: data = [ * #6.37(bstr .size 16) ]
	dev.AddAttribute("devices", nil)

	return dev
}

// =====================================================================
func NewPowerrelay(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("powerrelay.basic")
	dev.Address = addr

	// -------- Attributes --------
	// State of the relay (true=on false=off) | type: data = bool
	dev.AddAttribute("power", false)

	// -------- Methods --------
	// Switch on the relay
	defaultTurnOn := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOn method: turn_on")
		return nil
	}
	// Switch off the relay
	defaultTurnOff := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOff method: turn_off")
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)

	return dev
}

// =====================================================================
func NewPowerrelayToggle(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("powerrelay.toggle")
	dev.Address = addr

	// -------- Attributes --------
	// State of the relay (true=on false=off) | type: data = bool
	dev.AddAttribute("power", false)

	// -------- Methods --------
	// Switch on the relay
	defaultTurnOn := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOn method: turn_on")
		return nil
	}
	// Switch off the relay
	defaultTurnOff := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultTurnOff method: turn_off")
		return nil
	}
	// Toggle relay state
	defaultToggle := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultToggle method: toggle")
		return nil
	}
	dev.AddMethod("turn_on", defaultTurnOn)
	dev.AddMethod("turn_off", defaultTurnOff)
	dev.AddMethod("toggle", defaultToggle)

	return dev
}

// =====================================================================
func NewRaingauge(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("raingauge.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Real-time amount of rainfall | type: data = number | unit: mm
	dev.AddAttribute("rain", 0.0)
	// Accumulated precipitation | type: data = number | unit: mm
	dev.AddAttribute("accumulated", 0.0)

	return dev
}

// =====================================================================
func NewScale(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("scale.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Weight | type: data = number | unit: gram
	dev.AddAttribute("weight", 0.0)

	return dev
}

// =====================================================================
func NewScenario(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("scenario.basic")
	dev.Address = addr

	// -------- Attributes --------
	// State of scenario (true=enabled false=disabled) | type: data = bool
	dev.AddAttribute("enabled", false)
	// Fancy properties defined and managed by the scenario, if any | type: data = { * (tstr => tstr) }
	dev.AddAttribute("properties", nil)

	// -------- Methods --------
	// Enable the scenario
	defaultEnable := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultEnable method: enable")
		return nil
	}
	// Disable the scenario
	defaultDisable := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultDisable method: disable")
		return nil
	}
	// Run the scenario (if enabled)
	defaultRun := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: options
		slog.Info("defaultRun method: run", "args", args)
		return nil
	}
	// Abort the scenario (if running)
	defaultAbort := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultAbort method: abort")
		return nil
	}
	dev.AddMethod("enable", defaultEnable)
	dev.AddMethod("disable", defaultDisable)
	dev.AddMethod("run", defaultRun)
	dev.AddMethod("abort", defaultAbort)

	return dev
}

// =====================================================================
func NewShutter(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("shutter.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Ongoing action of the shutter | type: data = "up" / "down" / "stop"
	dev.AddAttribute("action", nil)

	// -------- Methods --------
	// Up the shutter
	defaultUp := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultUp method: up")
		return nil
	}
	// Down the shutter
	defaultDown := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultDown method: down")
		return nil
	}
	// Stop ongoing action of the shutter
	defaultStop := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultStop method: stop")
		return nil
	}
	dev.AddMethod("up", defaultUp)
	dev.AddMethod("down", defaultDown)
	dev.AddMethod("stop", defaultStop)

	return dev
}

// =====================================================================
func NewShutterPosition(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("shutter.position")
	dev.Address = addr

	// -------- Attributes --------
	// Ongoing action of the shutter | type: data = "up" / "down" / "stop"
	dev.AddAttribute("action", nil)
	// Level of aperture of the shutter | type: data = (0..100) / (0.0..100.0) | unit: %
	dev.AddAttribute("position", 0.0)

	// -------- Methods --------
	// Up the shutter
	defaultUp := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultUp method: up")
		return nil
	}
	// Down the shutter
	defaultDown := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultDown method: down")
		return nil
	}
	// Stop ongoing action of the shutter
	defaultStop := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultStop method: stop")
		return nil
	}
	// Change the position of the shutter
	defaultSetPosition := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: position
		slog.Info("defaultSetPosition method: set_position", "args", args)
		return nil
	}
	dev.AddMethod("up", defaultUp)
	dev.AddMethod("down", defaultDown)
	dev.AddMethod("stop", defaultStop)
	dev.AddMethod("set_position", defaultSetPosition)

	return dev
}

// =====================================================================
func NewSoundmeter(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("soundmeter.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Sound intensity | type: data = number | unit: dB
	dev.AddAttribute("sound", 0.0)

	return dev
}

// =====================================================================
func NewSwitch(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("switch.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Position of the switch (true=on false=off) | type: data = bool
	dev.AddAttribute("position", false)

	return dev
}

// =====================================================================
func NewThermometer(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("thermometer.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Temperature | type: data = number | unit: Cel
	dev.AddAttribute("temperature", 0.0)

	return dev
}

// =====================================================================
func NewTts(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("tts.basic")
	dev.Address = addr

	// -------- Methods --------
	// Say message
	defaultSay := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: msg, lang, voice
		slog.Info("defaultSay method: say", "args", args)
		return nil
	}
	dev.AddMethod("say", defaultSay)

	return dev
}

// =====================================================================
func NewWindgauge(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("windgauge.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Strength of the wind | type: data = number | unit: m/s
	dev.AddAttribute("wind_strength", 0.0)
	// Direction of the wind | type: data = (0..360) / (0.0..360.0) | unit: °
	dev.AddAttribute("wind_angle", 0.0)
	// Strength of gusts | type: data = number | unit: m/s
	dev.AddAttribute("gust_strength", 0.0)
	// Direction of gusts | type: data = (0..360) / (0.0..360.0) | unit: °
	dev.AddAttribute("gust_angle", 0.0)

	return dev
}

// =====================================================================
func NewWindow(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("window.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Position of the window (true=open false=close) | type: data = bool
	dev.AddAttribute("position", false)

	// -------- Methods --------
	// Open the window
	defaultOpen := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultOpen method: open")
		return nil
	}
	// Close the window
	defaultClose := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultClose method: close")
		return nil
	}
	dev.AddMethod("open", defaultOpen)
	dev.AddMethod("close", defaultClose)

	return dev
}

// =====================================================================
func NewWorktop(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("worktop.basic")
	dev.Address = addr

	// -------- Attributes --------
	// Ongoing action of the worktop | type: data = "up" / "down" / "stop"
	dev.AddAttribute("action", nil)

	// -------- Methods --------
	// Up the worktop
	defaultUp := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultUp method: up")
		return nil
	}
	// Down the worktop
	defaultDown := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultDown method: down")
		return nil
	}
	// Stop ongoing action of the worktop
	defaultStop := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("defaultStop method: stop")
		return nil
	}
	dev.AddMethod("up", defaultUp)
	dev.AddMethod("down", defaultDown)
	dev.AddMethod("stop", defaultStop)

	return dev
}

