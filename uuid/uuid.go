package uuid

import (
	"fmt"
	"math/big"

	"github.com/google/uuid"
)

var (
	Nil = UUID{uuid.Nil}
)

// UUID extends uuid.UUID
type UUID struct {
	uuid.UUID
}

// New generates a new UUID and returns it as a custom UUID
func New() UUID {
	return UUID{uuid.New()}
}

func Random() UUID {
	return UUID{uuid.New()}
}

func RandomBase(digit int) (UUID, error) {
	if digit < 1 || digit > 8 {
		return UUID{}, fmt.Errorf("digit must be between 1 and 8")
	}
	u := uuid.New()
	// zero out the last 4 bytes of the UUID
	for i := (16 - digit); i < 16; i++ {
		u[i] = 0
	}
	return UUID{u}, nil
}

func FromString(s string) (UUID, error) {
	u, err := uuid.Parse(s)
	if err != nil {
		return UUID{}, err
	}
	return UUID{u}, nil
}

func FromBytes(b []byte) (UUID, error) {
	u, err := uuid.FromBytes(b)
	if err != nil {
		return UUID{}, err
	}
	return UUID{u}, nil
}

func (obj UUID) ToBytes() []byte {
	return obj.UUID[:]
}

func (obj UUID) Add(n int64) (UUID, error) {
	// Convert UUID to big.Int
	myBigInt := new(big.Int).SetBytes(obj.UUID[:])

	// Add n to the big.Int
	myBigInt.Add(myBigInt, big.NewInt(n))

	// Convert big.Int back to UUID
	var u uuid.UUID
	b := myBigInt.Bytes()
	copy(u[16-len(b):], b)

	return UUID{u}, nil
}

func (obj UUID) Sub(n int64) (UUID, error) {
	return obj.Add(-n)
}
