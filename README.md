# xAAL Core for Golang

This is a [xAAL](http://recherche.imt-atlantique.fr/xaal/) stack implementation in Golang.

It contains the following components:

- uuid: a simple uuid toolkit.
- xaal: the xAAL core implementation.
- schemas: xAAL devices generated from the project schemas.

## Usage

```go
package main

import (
  "gitlab.imt-atlantique.fr/xaal/code/go/core/schemas"
  "gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
  "gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

func main() {
  eng := xaal.NewEngine()
  eng.Start()

  addr, _ := uuid.Random()
  lamp := schemas.NewLamp(addr)
  lamp.Info = "Fake (Go)lamp from schemas"
  lamp.VendorID = "RaMBO"
  lamp.Dump()

  light := lamp.GetAttribute("light")

  // Switch on the lamp
  turnOn := func(xaal.MessageBody) *xaal.MessageBody {
    light.SetValue(true)
    return nil
  }
  // Switch off the lamp
  turnOff := func(xaal.MessageBody) *xaal.MessageBody {
    light.SetValue(false)
    return nil
  }
  lamp.SetMethod("turn_on", turnOn)
  lamp.SetMethod("turn_off", turnOff)

  eng.AddDevice(lamp)
  eng.Run()
}
```

## License

This project is licensed under the LGPLv3 License.

## Authors

- Jérôme Kerdreux / IMT Atlantique 2024
